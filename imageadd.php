<?php

$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$path = 'files/'; // upload directory

// print_r($_FILES);die;

foreach ($_FILES['image_name']['name'] as $id => $val) {
    if (!empty($_POST['sports_id']) || $_FILES['image_name']) {
        $img = $_FILES['image_name']['name'][$id];
        $tmp = $_FILES['image_name']['tmp_name'][$id];

        // get uploaded file's extension
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

        // can upload same image using rand function
        $final_image = rand(1000, 1000000) . $img;

        // check's valid format
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);

            // print_r($_POST);die;
            if (move_uploaded_file($tmp, $path)) {
                $sports_id = $_POST['sports_id'];

                include 'dbconnection.php';

                $sql = ("INSERT INTO `sport_gallery`(`sports_id`, `image_name`) 
                VALUES ('" . $sports_id . "','" . $path . "')");

                $result = mysqli_query($connection, $sql);

                // print_r($result);

                // if (mysqli_query($connection, $sql)) {
                //     $response = [
                //         'status' => 'ok',
                //         'success' => true,
                //         'message' => 'Image uploaded successfully !!'
                //     ];
                //     echo json_encode($response);
                // } else {
                //     $response = [
                //         'status' => 'ok',
                //         'success' => false,
                //         'message' => 'Image uploading failed !!'
                //     ];
                //     echo json_encode($response);
                // }
            }
        } else {
            echo 'invalid file type';
        }
    }
}

// print_r(!empty($result));
if (!empty($result)) {
    $response = [
        'status' => 'ok',
        'success' => true,
        'message' => 'Image/images uploaded successfully !!'
    ];
    echo json_encode($response);
}

// die;
