$(document).ready(function () {
    categoryList();
});

// Multiple images preview in modal
$(function() {
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img width="100px" style="margin: 5px;" height="100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    $('#image_name').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});

// Listing category API
function categoryList() {
    $.ajax({
        url: BASE_URL + "categorylist.php",
        type: 'GET',
        success: function (data) {
            var response = JSON.parse(data);
            console.log(response);
            // alert("HELLO");
            var tr = "";

            let count = 0;
            for (var i = 0; i < response.length; i++) {
                var sports_category = response[i].sports_category.toUpperCase();
                var sports_id = response[i].sports_id;

                tr += '<tr>';

                tr += '<th>' + ++count + '</th>';

                tr += '<th>' + sports_category + '</th>';

                tr += '<th><div class="d-flex" style="align-items: center; justify-content: center">';

                tr += '<button type="button"><i class="material-icons" style="font-size:24px" onclick=addImage("' + sports_id + '")>add_a_photo</i></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                tr += '<button type="button"><i class="fas fa-images" style="font-size:24px" onclick="myFunction(' + sports_id + ')"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                tr += '<button type="button" data-bs-toggle="modal" data-bs-target="#editSportsTypeModal" onclick="viewCategory(' + sports_id + ')"><i class="fas fa-edit" style="font-size:24px"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                tr += '<button class="m-1 delete" onclick="deleteCategory(' + sports_id + ')" style="font-weight: bold;">Delete</button>';

                tr += '</div></th>';

                tr += '</tr>';
            }
            $('#sports_data').html(tr);
            $('#sportstable').DataTable();
        }
    });
}

function myFunction(sports_id) {
    location.replace("galleryindex.php/" + sports_id)
}

// ADD category API
$(document).ready(function () {
    $("#addSportsForm").validate({
        rules: {
            sports_category: {
                required: true
            }
        },
        messages: {
            sports_category: {
                required: "Please enter sports category it can not be left blank !!"
            }
        },
        submitHandler: function (form) {
            var sports_category = $('#sports_category').val();

            $.ajax({
                url: "categoryadd.php",
                type: "POST",
                data: {
                    sports_category: sports_category
                },
                success: function (data) {
                    var response = JSON.parse(data);
                    $('#addSportsTypeModal').modal('hide');
                    categoryList();
                    console.log(response.message);
                }
            });
            $('#addSportsTypeModal').on('hidden.bs.modal', function () {
                $(this).find('form').trigger('reset');
            });
        }
    });
});

// Edit category API
$(document).ready(function () {
    $("#editSportsForm").validate({
        rules: {
            sports_category: {
                required: true,
            }
        },
        messages: {
            sports_category: {
                required: "Please enter sports category it can not be left blank !!"
            }
        },
        submitHandler: function (form) {
            var sports_category = $('.edit_category #sports_category').val();
            var sports_id = $('.edit_category #sports_id').val();
            $.ajax({
                url: 'categoryedit.php',
                type: 'POST',
                data: {
                    sports_category: sports_category,
                    sports_id: sports_id
                },
                success: function (data) {
                    console.log(data);
                    var response = JSON.parse(data);
                    $('#editSportsTypeModal').modal('hide');
                    categoryList();
                    console.log(response.message);
                }
            });
            $('#editSportsTypeModal').on('hidden.bs.modal', function () {
                $(this).find('form').trigger('reset');
            });
        },
    });
});

// View category API
function viewCategory(sports_id = 2) {
    $.ajax({
        url: 'categoryview.php',
        type: 'GET',
        data: {
            sports_id: sports_id
        },
        success: function (data) {
            var response = JSON.parse(data);
            $('.edit_category #sports_category').val(response.sports_category);
            $('.edit_category #sports_id').val(response.sports_id);
        }
    });
}

// Delete category API 
function deleteCategory(sports_id) {
    if (confirm("Are you sure you want to delete?")) {
        $.ajax({
            url: 'categorydelete.php',
            type: 'GET',
            data: {
                sports_id: sports_id
            },
            success: function (data) {
                var response = JSON.parse(data);
                categoryList();
                console.log(response.message);
            }
        });
    }
    return false;
}

// Add image function
function addImage(sports_id) {
    $('#addImageModal').modal('show');
    $('.sports_id').val(sports_id);
}

// Add image API
$(document).ready(function () {
    $("#addImageForm").validate({
        rules: {
            image_name: {
                required: true
            },
        },
        messages: {
            image_name: {
                required: "Please upload a valid image. It can not be left blank !!"
            },
        },
        submitHandler: function (form) {
            $.ajax({
                url: "imageadd.php",
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#err").fadeOut();
                },
                success: function (data) {
                    var response = JSON.parse(data);
                    $('#addImageModal').modal('hide');
                    console.log(response.message);
                },
            });
            $('#addImageModal').on('hidden.bs.modal', function () {
                // $('.image_preview').removeAttr('src');
                var old_html = $("#gallery").html();
                $("#gallery").html(old_html);
                $(this).find('form').trigger('reset');
            })
        }
    });

});




