<!DOCTYPE html>
<html lang="en">
<?php include 'config.php'; ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">

    <!-- <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->

    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <style>
        .zoom {
            padding: 10px;

            transition: transform .2s;
        }
        .zoom:hover {

            transform: scale(2.3);
        }
    </style>

    <title>Document</title>
</head>

<body>
    <br>
    <!-- Gallery Listing Page  -->
    <div class="container">
        <div id="gallerylisting1">

        </div>
        <!-- <div class="card-body" id="gallerylisting2">
        </div> -->
        <table id="imagetable" class="table table-bordered table-striped" style="text-align: center;font-size:larger;">

        </table>
    </div>

</body>

<script>
    $(document).ready(function() {
        galleryList(sports_id);
    });

    var url = window.location.pathname;
    var sports_id = url.substring(url.lastIndexOf('/') + 1);

    // Gallery List API
    function galleryList(sports_id) {
        $.ajax({
            url: "<?php echo $base_url; ?>gallerylist.php",
            type: 'GET',
            data: {
                sports_id: sports_id,
            },
            success: function(data) {
                var response = JSON.parse(data);

                var count = 0;
                var tr1 = "";
                var tr2 = "";
                var sports_category = response[0].sports_category.toUpperCase();
                var count = 0;

                tr1 += '<div>';
                tr1 += '<button type="button"><a href="<?php echo $base_url; ?>index.php">Home Page</a></button>';
                tr1 += '</div>';
                tr1 += '<div>';
                tr1 += '<h1 style="text-align: center;">IMAGES OF ' + sports_category + ' SPORT</h1>';
                tr1 += '</div>';

                // tr2 += '<table id="imagetable" class="table table-bordered table-striped" style="text-align: center;font-size:larger;">';
                tr2 += '<thead style="text-transform:uppercase;">';
                tr2 += '<tr>';
                tr2 += '<th width="20%">S. No.</th>';
                tr2 += '<th width="50%">Image</th>';
                tr2 += '<th width="30%">Action</th>';
                tr2 += '</tr>';
                tr2 += '</thead>';
                tr2 += '<tbody>';

                for (var i = 0; i < response.length; i++) {
                    var image_name = response[i].image_name;
                    var sport_gallery_id = response[i].sport_gallery_id;

                    tr2 += '<tr>';
                    tr2 += '<td>' + ++count + '</td>';
                    tr2 += '<td><img class="zoom" src="<?php echo $base_url; ?>' + image_name + '" width="150px" height="150px"></td>';
                    tr2 += '<td><button type="button" id="deleteimage" class="deleteimage" name="deleteimage" onclick="imageDelete(' + sport_gallery_id + ')">Delete</button></td>';
                    tr2 += '</tr>';
                }

                tr2 += '</tbody>';
                // tr2 += '</table>';

                $('#gallerylisting1').html(tr1);
                $('#imagetable').html(tr2);
                $('#imagetable').DataTable();


            }
        });
    }

    // Image delete API
    function imageDelete(sport_gallery_id) {
        if (confirm("Are you sure you want to delete?")) {
            $.ajax({
                url: '<?php echo $base_url; ?>imagedelete.php',
                type: 'GET',
                data: {
                    sport_gallery_id: sport_gallery_id,
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    galleryList(sports_id);
                    console.log(response.message);
                }
            });
        }
        return false;
    }
</script>