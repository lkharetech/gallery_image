<?php
include 'dbconnection.php';

$sports_id = $_GET['sports_id'];

$join = "SELECT sports.sports_id, sport_gallery.image_name
FROM sports
RIGHT JOIN sport_gallery ON sports.sports_id = sport_gallery.sports_id WHERE sports.sports_id = $sports_id";

$result = mysqli_query($connection, $join);
$data = [];
while($fetch=mysqli_fetch_assoc($result)) {
    $data[]=$fetch;
}

for ($i=0; $i < count($data); $i++) { 
    unlink($data[$i]['image_name']);    
}

// Delete sport_gallery data 
$sqlcategory = "DELETE FROM `sport_gallery` WHERE `sports_id`='$sports_id'";
$result1 = mysqli_query($connection, $sqlcategory);

// Delete sports data
$sql = "DELETE FROM `sports` WHERE `sports_id`='$sports_id'";

if(mysqli_query($connection, $sql)){
    $response = [
        'status'=>'ok',
        'success'=>true,
        'message'=>'Record deleted succesfully!',
    ];
    print_r(json_encode($response));
} else {
    $response = [
        'status'=>'ok',
        'success'=>false,
        'message'=>'Record deletion failed!'
    ];
    print_r(json_encode($response));
} 
?>