<?php
include 'dbconnection.php';

$sports_id = $_GET['sports_id'];

$sql = "SELECT sports.sports_category, sport_gallery.image_name , sport_gallery.sport_gallery_id
            FROM sports
            INNER JOIN sport_gallery 
            ON sports.sports_id = sport_gallery.sports_id 
            WHERE sport_gallery.sports_id = $sports_id";

$result = mysqli_query($connection, $sql);
$data = [];

while ($fetch = mysqli_fetch_assoc($result)) {
    $data[] = $fetch;
}

print_r(json_encode($data));
