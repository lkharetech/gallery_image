<!DOCTYPE html>
<html lang="en">
<?php include 'config.php'; ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script> -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <!-- <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

    <title>Ajax Different sports images</title>
</head>

<body class="bg-dark">
    <!-- Main Page of sports category -->
    <div class="container mt-5 pt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1>Sports Gallery through AJAX(Image upload)</h1>
                    </div>
                    <div class="text-end">
                        <button id="add_button" type="button" data-bs-toggle="modal" data-bs-target="#addSportsTypeModal" class="btn btn-success"><i aria-hidden="true">ADD SPORTS CATEGORY</i></button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="sportstable" class="table table-bordered table-striped" style="text-align: center;font-size:larger;">
                    <thead style="text-transform:uppercase;">
                        <tr>
                            <th width="20%">S. No.</th>
                            <th width="40%">Sports Category</th>
                            <th width="40%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="sports_data">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>

</html>

<!-- ADD MODAL of new sports category -->
<div class="modal fade" id="addSportsTypeModal">
    <div class="modal-dialog">
        <form method="post" id="addSportsForm" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Sports</h4>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Add New Sports</label>
                        <input type="text" name="sports_category" id="sports_category" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Add">
                    <button class="btn btn-default" type="button" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- EDIT MODAL of new sports category  -->
<div class="modal fade" id="editSportsTypeModal">
    <div class="modal-dialog">
        <form method="post" id="editSportsForm" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Sport</h4>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body edit_category">
                    <div class="form-group">
                        <label>Edit Sport</label>
                        <input type="text" name="sports_category" id="sports_category" class="form-control">
                        <input type="hidden" id="sports_id" name="sports_id" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Save" data-bs-dismiss="modal">
                    <button class="btn btn-default" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- ADD IMAGE MODAL of sports category -->
<div id="addImageModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="addImageForm" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">Add Image</div>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body add_image">
                    <div class="form-group">
                        <label>Add Image</label>
                        <input type="file" id="image_name" name="image_name[]" class="form-control" multiple>
                        <span class="text-muted">Only .jpg, .png, .gif file allowed</span>
                        <br>
                        <div class="gallery"></div>
                        <!-- <img src="" id="image_preview" class="image_preview" alt="image" width="150" height="150"> -->
                        <input type="hidden" id="sports_id" name="sports_id" class="form-control sports_id">
                    </div>
                    <div id="err"></div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Save">
                    <button class="btn btn-default" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var BASE_URL = "<?php echo $base_url; ?>";
</script>

<script src="main.js">
</script>